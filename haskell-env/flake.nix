{
  description = "Simple haskell nix flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

    # Flake outputs
  outputs = { self, nixpkgs }: let

    allSystems = [ "x86_64-linux" "x86_64-darwin" ];

    # Helper to provide system-specific attributes
    forAllSystems = f: nixpkgs.lib.genAttrs allSystems (system: f {
      pkgs = import nixpkgs { inherit system; };
    });

  in {
    devShells = forAllSystems ({ pkgs }: {
      default = pkgs.haskellPackages.shellFor {
        packages = hpkgs: [];
        nativeBuildInputs = [
          #pkgs.cabal-install
          pkgs.ghc
          pkgs.haskell-language-server
        ];
        withHoogle = true;
      };
    });
  };
}
