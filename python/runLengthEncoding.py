# Run-length encoding is a fast and simple method of encoding strings. The basic idea is to represent repeated successive characters as a single 
# count and character. For example, the string "AAAABBBCCDAA" would be encoded as "4A3B2C1D2A".

# Implement run-length encoding and decoding. You can assume the string to be encoded have no digits and consists solely of alphabetic characters. 
# You can assume the string to be decoded is valid.

def run_length_encoding(string):
    
    if not string:
        return ''
    
    result = ''
    current_chr = string[0]
    current_count = 1

    for i, chr in enumerate(string, 1):
        if chr == current_chr:
            current_count += 1
        else:
            result += str(current_count) + current_chr
            current_chr = chr
            current_count = 1
    result += str(current_count) + current_chr
    return result

def run_length_decoding(string):
    result = ''
    count = 0
    for chr in string:
        if chr.isdigit():
            count = int(chr)
        else:
            result += chr * count
    return result
        

print(run_length_encoding('AAAABBBCCDAA'))
print(run_length_decoding(run_length_encoding('AAAABBBCCDAA')))
print(run_length_decoding(run_length_encoding('AAABD')))