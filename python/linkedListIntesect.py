from sys import exit

class Node:
    def __init__(self, data) -> None:
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self) -> None:
        self.head = None

    def print(self) -> None:
        node = self.head
        while node is not None:
            print(node.data)
            node = node.next

    def __length(self, node):
        if node is None:
            return 0
        
        return 1 + self.__length(node.next)
    
    def length(self):
        return self.__length(self.head)


    def append(self, data) -> None:
        if self.head is None:
            self.head = Node(data)
        else:
            end = self.head
            while end.next is not None:
                end = end.next
            end.next = Node(data)
            

def main():

    A = LinkedList()

    A.append(3)
    A.append(7)
    A.append(8)
    A.append(10)


    A.print()

    print(A.length())

if __name__ == '__main__':
    exit(main())