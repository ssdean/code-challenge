import sys

def unclosedParenthesesCount(str):
    stack = []
    count = 0

    for item in str:
        if item == '(':
            stack.append('(')
        else:
            if len(stack) > 0:
                stack.pop()
            else:
                count += 1

    
    count = count + len(stack)
    return count

def main():
    str1 = '()())()'

    a = unclosedParenthesesCount(str1)
    print(a)

    str2 = ')('

    b = unclosedParenthesesCount(str2)
    print(b)

    return 0

if __name__ == '__main__':
    sys.exit(main())
