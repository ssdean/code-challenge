# This is your coding interview problem for today.

# This problem was asked by Snapchat.

# Given an array of time intervals (start, end) for classroom lectures (possibly overlapping), find the minimum number of rooms required.

# For example, given [(30, 75), (0, 50), (60, 150)], you should return 2.

intervals = [(30, 75), (0, 90), (80, 150), (90, 100)]

rooms = 1
min = intervals[0][0]
max = intervals[0][1]

for slot in intervals[1:]:

    if slot[0] > min and slot[0] < max:
        rooms += 1
    elif slot[1] > min and slot[1] < max:
        rooms += 1
    elif slot[0] < min and slot[1] > max:
        rooms += 1


    if slot[0] < min:
        min = slot[0]

    if slot[1] > max:
        max = slot[1]
    
print(rooms)
    

         