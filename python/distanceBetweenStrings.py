# The edit distance between two strings refers to the minimum number of character insertions, deletions, 
# and substitutions required to change one string to the other. For example, the edit distance between “kitten” and “sitting” is 
# three: substitute the “k” for “s”, substitute the “e” for “i”, and append a “g”.

# Given two strings, compute the edit distance between them.

def distance_between_strings(s1, s2):

    if not s1 or not s2:
        return

    string1 = ''
    string2 = ''
    diff = 0
    if len(s1) > len(s2):
        string1 = s1
        string2 = s2
    else:
        string1 = s2
        string2 = s1

    diff = len(string1) - len(string2)

    for i, char in enumerate(string2):
        if char != string1[i]:
            diff += 1
    
    return diff

print(distance_between_strings('kitten', 'sitting'))
print(distance_between_strings('apple', 'apples'))
print(distance_between_strings('carrot', 'car'))
print(distance_between_strings('pat', 'pot'))


    



    