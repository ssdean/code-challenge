# Given a string of round, curly, and square open and closing brackets, return whether the brackets are balanced (well-formed).

# For example, given the string "([])[]({})", you should return true.

# Given the string "([)]" or "((()", you should return false.


def balanced_brackets(string):
    values = {'(': ')', '[': ']', '{': '}'}
    stack = []

    if len(string) < 1 or len(string) % 2 != 0:
        return False

    for item in string:
        if item in values.keys():
            stack.append(item)
        else:
            if not stack or values[stack.pop()] != item:
                return False
    
    if not stack:
        return True
    
    return False
        
print(balanced_brackets("([])[]({})"))
print(balanced_brackets("([)]"))
print(balanced_brackets("((()"))
print(balanced_brackets("(([{[]{[()]}}])())"))
print(balanced_brackets(""))
print(balanced_brackets("[]"))
print(balanced_brackets("}{"))


