#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <CUnit/Basic.h>
#include "../include/generaltree.h"

// CUnit test suite initialization function
int init_suite(void) {
    return 0;
}

// CUnit test suite cleanup function
int clean_suite(void) {
    return 0;
}

// Test case for checking max_depth function
void test_insert() {
    struct node *root = create_node('\0');
    insert_word(&root, "the");
    insert_word(&root, "tree");
    insert_word(&root, "them");
    insert_word(&root, "that");
    insert_word(&root, "apple");
    insert_word(&root, "ant");
    insert_word(&root, "bean");

    int depth = max_depth(root);
    CU_ASSERT_EQUAL(depth, 2); // Update this with the expected depth of your tree
}

// Main function running CUnit tests
int main() {
    CU_pSuite pSuite = NULL;

    // Initialize the CUnit test registry
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    // Add a suite to the registry
    pSuite = CU_add_suite("autocomplete_suite", init_suite, clean_suite);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Add the test case to the suite
    if (NULL == CU_add_test(pSuite, "test_insert", test_insert)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Run all tests using the CUnit Basic interface
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}
