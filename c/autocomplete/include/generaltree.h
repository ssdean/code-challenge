#ifndef GENERAL_TREE_H
#define GENERAL_TREE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NODES 52 // All uppercase and lowercase letters
#define WORD_LEN 32

struct node {
    char value;
    int word_end;
    struct node *first_child;
    struct node *next_sibling;
};

struct word_array {
    char *arr;
    int len;
};

// Function prototypes
struct node *create_node(char value);
struct node *child_contains_char(struct node *n, char c);
void insert_word(struct node **n, char *word);
int tree_depth_helper(struct node *n);
int tree_depth(struct node *n);
int child_count(struct node *n);
int max_depth(struct node *n);
int word_count_helper(struct node *n);
int word_count(struct node *n);
void find_prefixed_words(struct node *n, char *prefix);
void print_tree(struct node *n);

#endif