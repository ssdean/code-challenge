#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NODES 52 // All uppercase and lowercase letters
#define WORD_LEN 32

struct node {
    char value;
    int word_end;
    struct node *first_child;
    struct node *next_sibling;
};

struct word_array {
    char *arr;
    int len;
};

struct node *create_node(char value) {
    struct node *new_node = malloc(sizeof(struct node));
    if (new_node == NULL) {
        printf("Memory allocation failed\n");
        return NULL;
    }

    new_node->value = value;
    new_node->word_end = 0;
    new_node->first_child = NULL;
    new_node->next_sibling = NULL;

    return new_node;
}

struct node *child_contains_char(struct node *n, char c) {
    if (n == NULL || n->first_child == NULL) {
        return NULL;
    }

    struct node *current_node = n->first_child;

    if (current_node->value == c) {
        return current_node;
    }

    while (current_node != NULL) {
        if (current_node->value == c) {
            return current_node;
        }
        current_node = current_node->next_sibling;
    }
    return NULL;
}

void insert_word(struct node **n, char *word) {
    if ((*n) == NULL || strcmp(word, "\0") == 0) {
        return;
    }

    int i = 0;
    struct node *current_node = child_contains_char((*n), word[i]);

    if (current_node != NULL) {
        if (strlen(word) == 1) {
            current_node->word_end = 1;
        }
        insert_word(&current_node, &word[++i]);
    }
    else {
        current_node = create_node(word[i++]);
        if (strlen(word) == 1) {
            current_node->word_end = 1;
        }
        if ((*n)->first_child == NULL) {
            (*n)->first_child = current_node;
            insert_word(&current_node, &word[i]);
        }
        else {
            struct node *tmp = (*n)->first_child;
            while (tmp->next_sibling != NULL) {
                tmp = tmp->next_sibling;
            }
            tmp->next_sibling = current_node;
            insert_word(&current_node, &word[i]);
        }
    }
}

int tree_depth_helper(struct node *n) {
    if (n == NULL || n->first_child == NULL) {
        return 0;
    }

    int count = 1;

    struct node *current_node = n->first_child;
    while (current_node->next_sibling != NULL) {
        current_node = current_node->next_sibling;
        if (current_node->first_child != NULL) {
            count = count + tree_depth_helper(current_node->first_child);
        }
    }

    return count;
}

int tree_depth(struct node *n) {
    if (n == NULL) {
        return 0;
    }

    return 1;
}

int child_count(struct node *n) {
    if (n == NULL || n->first_child == NULL) {
        return 0;
    }

    int count = 0;
    struct node *current_node = n->first_child;
    while (current_node != NULL) {
        current_node = current_node->next_sibling;
    }

    return count;
}

int max_depth(struct node *n) {
    if (n == NULL) {
        return 0;
    }

    int max_child_depth = 0;
    struct node *child = n->first_child;

    while (child != NULL) {
        int child_depth = max_depth(child);
        if (child_depth > max_child_depth) {
            max_child_depth = child_depth;
        }
        child = child->next_sibling;
    }

    return 1 + max_child_depth;
}

int word_count_helper(struct node *n) {
    if (n == NULL) {
        return 0;
    }

    int counter = 0;

    if (n->word_end > 0) {
        counter++;
    }

    if (n->first_child != NULL) {
        counter = counter + word_count_helper(n->first_child);
    }

    if (n->next_sibling != NULL) {
        counter = counter + word_count_helper(n->next_sibling);
    }

    return counter;
}

int word_count(struct node *n) {
    if (n == NULL) {
        return 0;
    }

    int count = 0;

    if (n->word_end > 0) {
        count++;
    }

    count = count + word_count_helper(n->first_child);

    return count;

}

void find_prefixed_words(struct node *n, char *prefix) {
    if (n == NULL || prefix == NULL) {
        return;
    }

    struct node *current_node = child_contains_char(n, prefix[0]);

    if (current_node == NULL) {
        printf("No words\n");
        return;
    }

    int i = 1;
    while (current_node != NULL && strlen(&prefix[i]) > 0) {
        current_node = child_contains_char(current_node, prefix[i++]);
    }

    int count = word_count(current_node);

    struct word_array *words = malloc(sizeof(struct word_array));
    words->arr = (char*) malloc(count * WORD_LEN);
    words->len = count;

    memset(words->arr, '\0', count * WORD_LEN);

    for (int j = 0; j < count; j++) {
        strcpy(&words->arr[j * WORD_LEN], prefix);
    }

    for (int k = 0; k < count * WORD_LEN; k++) {
        printf("%c", words->arr[k]);
    }
    puts("");
    printf("Count: %d\n", count);

}

void print_tree(struct node *n) {
    if (n == NULL) {
        return;
    }

    printf("%c", n->value);

    print_tree(n->first_child);
    print_tree(n->next_sibling);
}

int main() {

    struct node *root = create_node('\0');
    insert_word(&root, "the");
    insert_word(&root, "tree");
    insert_word(&root, "them");
    insert_word(&root, "that");
    insert_word(&root, "apple");
    insert_word(&root, "ant");
    insert_word(&root, "bean");



    print_tree(root);
    puts("");

    find_prefixed_words(root, "Th");

    int counter = word_count(root);

    printf("Word Count: %d\n", counter);

    int depth = max_depth(root);
    printf("Tree Depth: %d\n", depth);

    return 0;
}