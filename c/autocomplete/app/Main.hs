module Main where

data Tree a = Node a [Tree a]
    deriving (Show)

-- insert :: Ord a -> Tree a -> Tree a
-- insert x 

tree :: Tree Char
tree = Node '#' 
    [ Node 'c' []
    , Node 'a' []
    , Node 't' [ Node 'r' [] ]]

main :: IO ()
main = do 
    putStrLn "Hello, Haskell!"
    print tree
