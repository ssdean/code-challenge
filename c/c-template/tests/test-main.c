#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <CUnit/Basic.h>
#include "include.h"

// CUnit test suite initialization function
int init_suite(void) {
    return 0;
}

// CUnit test suite cleanup function
int clean_suite(void) {
    return 0;
}

// Test case for checking max_depth function
void test_example(void) {
    int x = 4;
    int y = x * 2;
    int res = 8;
    CU_ASSERT_EQUAL(y, res); // Update this with the expected depth of your tree
}

// Main function running CUnit tests
int main() {
    CU_pSuite pSuite = NULL;
    
    // Initialize the CUnit test registry
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    // Add a suite to the registry
    pSuite = CU_add_suite("example_suite", init_suite, clean_suite);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Add the test case to the suite
    if (NULL == CU_add_test(pSuite, "test_example", test_example)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Run all tests using the CUnit Basic interface
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}
