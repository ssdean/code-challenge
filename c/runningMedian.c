#include <math.h>
#include <stdio.h>

void ordered_insert(int *seq, int size, int item) {
  int res[size + 1];
  for (int i = 0; i < size; i++) {
    if (seq[i] <= item) {
      res[i] = seq[i];
    } else {
      res[i] = item;
      for (int j = size - 1; j > i; j--) {
        res[j] = seq[j];
        printf("ADDING: %d\n", seq[j]);
      }
      break;
    }
  }

  for (int i = 0; i < size + 1; i++) {
    printf("OUT: %d\n", res[i]);
  }
}

void running_median(int *sequence, int sequence_lenth) {
  for (int i = 0; i < sequence_lenth; i++) {
    if ((i + 1) % 2 == 0) {
      printf("%d is even\n", i + 1);
    } else {
      // printf("%d\n", (int)floor((float)i / 2));
    }
  }
}

int main() {

  // int seq[] = {2, 1, 5, 7, 2, 0, 5};
  int seq[] = {1, 2, 2, 3, 4};
  int seqlen = sizeof(seq) / sizeof(seq[0]);

  ordered_insert(seq, seqlen, 3);

  running_median(seq, seqlen);

  return 0;
}
